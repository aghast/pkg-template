# python: encoding=utf-8
""" tests/context.py

    Set up testing context. You should be able to relatively import this
    module from another tests/ module, and get going:

        from .context import <modulename>

"""
import sys
from os.path import abspath, dirname, join

import {{"%24s"|format(cookiecutter.project_slug)}} # pylint: disable=unused-import
import semver                   # pylint: disable=unused-import

# This assumes that this file has relative path $ROOT/tests/context.py.
# It resolves "$ROOT/tests/.." to an absolute path to get $ROOT, and
# adds that directory to sys.path. The result is that imports should
# work on source files directly. If your source files are in a sub-dir,
# like $ROOT/source/pkg/module.py, then consider appending '/source' to
# the path being added here:
root_dir = abspath(join(dirname(__file__), '..'))
sys.path.insert(0, root_dir)
del sys, abspath, dirname, join, root_dir

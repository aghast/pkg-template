""" setup.py

    This is a dummy file with no contents. All "real" settings are in
    `pyproject.toml`. There are a few utilities that won't run correctly
    if there is not a `setup.py` file in the project, so this should
    satisfy them.

"""
import setuptools

setuptools.setup()

# python: encoding=utf-8
""" ./etc/runtests.py

    Run the test suite.

"""
import sys
import pytest

if __name__ == "__main__":
    sys.exit(pytest.main(['tests']))

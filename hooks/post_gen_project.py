# python: encoding=utf-8
""" hooks/post_gen_project.py

    Cookiecutter hook to run after project generation has been done by
    Jinja.

"""
import sys
import subprocess as subp


if sys.version_info < (3, 6):
    raise ImportError("Python version must be >= 3.6")


def hg(*args, **kwargs):                # pylint: disable=invalid-name
    """ Run an hg subcommand. """
    hg_args = {"stdin": subp.DEVNULL, **kwargs}
    hg_cmd = ['hg', *args]
    cp = subp.run(hg_cmd, check=True, **hg_args)
    return cp


def hg_init():
    """ Run "hg init" in the current directory. """
    hg("init")


def hg_add_paths():
    """ Add [paths] section to .hg/hgrc file. """
    repo = "{{cookiecutter.project_slug}}"
    # Important! The keys are the domain names.
    username = {
        "gitlab.com": "{{cookiecutter.gitlab_username}}",
        "github.com": "{{cookiecutter.github_username}}",
    }
    paths = ["[paths]"]
    for k, v in username.items():
        if not v:
            continue
        # Append strings like: "gitlab = git@gitlab.com:aghast/repo.git"
        paths.append(f"{k} = git@{k}:{v}/{repo}.git")

    if len(paths) < 2:
        print("WARNING: No repo links create. This repo cannot push.")
        return

    with open(".hg/hgrc", "w") as hgrc:
        print("\n".join(paths), file=hgrc)

def main():
    """ Run set up hg repository in current directory. """
    hg_init()
    hg_add_paths()

if __name__ == "__main__":
    main()
